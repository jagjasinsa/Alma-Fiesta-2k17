package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class FaceOffZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_off_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView66);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openFaceOffRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(FaceOffZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. Each contestant will be given a maximum time limit of 8 minutes.\n\n" +
                                    "2. The competition will be among different contestants.\n\n" +
                                    "3. The contestants are allowed to do anything like Standup comedy, Mimicry, mime, Impromptu or any other skill they wish to perform.\n\n" +
                                    "4. This is strictly a Solo act competition.\n\n" +
                                    "5. The language of act is strictly prohibited to English/Hindi.\n\n" +
                                    "6. The contestant’s performance is scrutinized and judged based upon the creativity, stage presence and interest shown in entertaining the crowd.\n\n" +
                                    "7. Microphones, lights and laptop to play sounds will be provided.\n\n" +
                                    "8. Instrumental music is allowed but the participants will have to bring their own instruments.\n\n" +
                                    "9. Naked flames or live animals are not allowed on stage.\n\n" +
                                    "10. Participants will have to bring their own music in CD/PD.\n\n" +
                                    "11. Sometimes the judges may ask the contestants to perform something on spot to study the skills of the contestant.\n\n" +
                                    "12. The top three performances judged the jury gets to be the winners of the competition. ")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openFaceOffCo(View view)
    {
        final SpannableString s = new SpannableString("Raghavendra : 9178523187" + "\n"+ "\n" + "Himanshu : 9587497460");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();
    }

}
