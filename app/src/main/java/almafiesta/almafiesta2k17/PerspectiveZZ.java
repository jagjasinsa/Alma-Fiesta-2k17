package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class PerspectiveZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perspective_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView69);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openPerspectiveRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(PerspectiveZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("It is a team event.\n" +
                                    "\n" +
                                    "\uF0B7 Each team will consist of 2-3 members.\n" +
                                    "\n" +
                                    "\uF0B7 Problem statement will be released soon on social networks.\n" +
                                    "\n" +
                                    "\uF0B7 Participants have to give a write up in pdf form and should be submitted before the given deadline.\n" +
                                    "\n" +
                                    "\uF0B7 Selected participants will be informed and they have to give a power point presentation on the problem statement during the fest.\n" +
                                    "\n" +
                                    "\uF0B7 Total time per team: 15 minutes - 10 minutes Presentation + 5 minutes\n" +
                                    "\n" +
                                    "Question/Answer session\n" +
                                    "\n" +
                                    "\uF0B7 All participants must bring their college identity cards when they come for final presentation.\n" +
                                    "\n" +
                                    "\uF0B7 Participants without identity cards will not be allowed to present even if they are selected for the finals.\n" +
                                    "\n" +
                                    "\uF0B7 Plagiarism will not be tolerated. Any participant violating this rule will be disqualified.\n" +
                                    "\n" +
                                    "\uF0B7 References should be mentioned clearly in the report as well as in the power point presentation(s).\n" +
                                    "\n" +
                                    "\uF0B7 The decision of the judges will be final and binding")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }


    public void openPerspectiveCo(View view)
    {
        final SpannableString s = new SpannableString("Saurabh : 8280328334" + "\n"+ "\n" + "Abhijeet : 8004592855");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();
    }

}
