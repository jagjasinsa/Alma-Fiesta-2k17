package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;

import com.eftimoff.viewpagertransformers.CubeOutTransformer;
import com.eftimoff.viewpagertransformers.FlipHorizontalTransformer;
import com.eftimoff.viewpagertransformers.RotateUpTransformer;
import com.eftimoff.viewpagertransformers.StackTransformer;
import com.eftimoff.viewpagertransformers.TabletTransformer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Home extends AppCompatActivity {
    ViewPager mViewPager;
    String file = "FileAll";
    String temp = "";
    String temp1 = "";
    String file1 = "dialog1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        //Adding dialog for swipe details
        try {
            FileInputStream fileInputStream = openFileInput(file1);
            int c;
            while ((c = fileInputStream.read()) != -1) {
                temp1 = temp1 + Character.toString((char) c);
            }
        } catch (FileNotFoundException e) {
            temp1 = "1";

        } catch (IOException e) {
            e.printStackTrace();
        }

        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("ALMA FIESTA 2K17")
                        .setContentText("13-15th January, 2017");

        Intent notificationIntent = new Intent(this, Splah.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

        if (temp1.equals("1")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!isFinishing()) {
                        new AlertDialog.Builder(Home.this)
                                .setTitle("!! STAR-NITE !!")
                                .setMessage("DO YOU WANT US TO REMIND YOU ABOUT STAR-NITE?" + '\n' + "Just press the OK button followed by a confirmation and we will do the rest for you.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        String date = "15/0/2017";
                                        String parts[] = date.split("/");
                                        int day = Integer.parseInt(parts[0]);
                                        int month = Integer.parseInt(parts[1]);
                                        int year = Integer.parseInt(parts[2]);
                                        Calendar calendar = Calendar.getInstance();
                                        calendar.set(Calendar.YEAR, year);
                                        calendar.set(Calendar.MONTH, month);
                                        calendar.set(Calendar.DAY_OF_MONTH, day);


                                        Intent intent = new Intent(Intent.ACTION_EDIT);
                                        intent.setType("vnd.android.cursor.item/event");
                                        intent.putExtra("beginTime", calendar.getTimeInMillis());
                                        intent.putExtra("allDay", true);
                                        intent.putExtra("endTime", calendar.getTimeInMillis());
                                        intent.putExtra("title", "Get ready for the Star-Nite, 'THE LOCAL TRAIN' TODAY!!! ");
                                        startActivity(intent);
                                    }
                                }).show();
                    }
                }
            });


            try {
                String s = "zeus";
                FileOutputStream fout = openFileOutput(file1, MODE_APPEND);
                fout.write(s.getBytes());
                fout.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (!isFinishing()) {
                        Calendar c = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                        String strDate = sdf.format(c.getTime());
                        if (strDate.equals("2016")) {
                            new AlertDialog.Builder(Home.this)
                                    .setTitle("INFORMATION")
                                    .setMessage("Merry Christmas and a Happy New Year. Best Wishes, Team Alma Fiesta")
                                    .setCancelable(false)
                                    .setPositiveButton("REDEFINING FESTIVITY", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).show();
                        } else {
                            new AlertDialog.Builder(Home.this)
                                    .setTitle("INFORMATION")
                                    .setMessage("Alma Fiesta 2k17, DAWN OF THE COSMIC ODYSSEY on 13-15th January this year.")
                                    .setCancelable(false)
                                    .setPositiveButton("REDEFINING FESTIVITY", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                        }
                                    }).show();
                        }

                    }
                }
            });
        }


        mViewPager = (ViewPager) findViewById(R.id.pager);

        try {
            FileInputStream fileInputStream = openFileInput(file);
            int c;
            while ((c = fileInputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
        } catch (FileNotFoundException e) {
            temp = "1";

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (temp.equals("1")) {
            mViewPager.setPageTransformer(true, new CubeOutTransformer());
        } else if (temp.equals("2")) {
            mViewPager.setPageTransformer(true, new TabletTransformer());
        } else if (temp.equals("3")) {
            mViewPager.setPageTransformer(true, new FlipHorizontalTransformer());
        } else if (temp.equals("4")) {
            mViewPager.setPageTransformer(true, new DepthPageTransformer(100));
        } else if (temp.equals("5")) {
            mViewPager.setPageTransformer(true, new StackTransformer());
        } else if (temp.equals("6")) {
            mViewPager.setPageTransformer(true, new RotateUpTransformer());
        } else mViewPager.setPageTransformer(true, new CubeOutTransformer());


        // mViewPager.setPageTransformer(true, new DepthPageTransformer(150));
        mViewPager.setAdapter(new SamplePagerAdapter(
                getSupportFragmentManager()));
    }

    public class SamplePagerAdapter extends FragmentPagerAdapter {

        public SamplePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new ZforZeus();
            } else if (position == 1) {
                return new Events();
            } else if (position == 2) {
                return new StarNight();
            } else if (position == 3) {
                return new Workshop();
            } else if (position == 4) {
                return new RegisterN();
            } else
                return new ContactUs();

        }

        @Override
        public int getCount() {
            return 6;
        }
    }
}
