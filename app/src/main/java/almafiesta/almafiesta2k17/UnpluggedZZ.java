package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class UnpluggedZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unplugged_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView13);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openUnpluggedRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(UnpluggedZZ.this)
                            .setTitle("RULES")
                            .setMessage("Rounds: 2 Rounds – Qualifying and Final\n" +
                                    "\n" +
                                    "Eligibility: Solo performance on any instrument\n" +
                                    "\n" +
                                    "Rules:\n" +
                                    "\n" +
                                    "1) Participants are not allowed to use laptops or mixers or any other digital modifiers.\n" +
                                    "\n" +
                                    "2) Drum kit will be provided by organizing team on stage for drummers.\n" +
                                    "\n" +
                                    "3) Maximum time limit on stage is 10 minutes. (amps on to amps off).\n" +
                                    "\n" +
                                    "4) Original compositions are welcome.\n" +
                                    "\n" +
                                    "5) Participant may add vocals but judging will only be on instrument playing.")
                            .setCancelable(false)
                            .setPositiveButton("GOT IT", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openUnCo(View view)
    {
        final SpannableString s = new SpannableString("Himaja : 9078814813" + "\n"+ "\n" + "Niteesh : 9078814446");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
