package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class UpBeatZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_beat_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView)findViewById(R.id.textView74);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }

    public void openUpBeatRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(UpBeatZZ.this)
                            .setTitle("RULES")
                            .setMessage("Rounds: 2 Rounds – Qualifying and Final\n" +
                                    "\n" +
                                    "Eligibility: Acoustic Group Performance\n" +
                                    "\n" +
                                    "Rules:\n" +
                                    "\n" +
                                    "1. Participants are not allowed to use laptops or mixers or any other digital modifiers.\n" +
                                    "\n" +
                                    "2. Only acoustic and semi - acoustic instruments are allowed. Synthesizer and acoustic bass guitar are allowed.\n" +
                                    "\n" +
                                    "3. Number of group members: 2(min) to 7(max)\n" +
                                    "\n" +
                                    "4. Each group will be allotted 5 minutes for sound check and 15 minutes for performance. Total on stage time is 20 minutes.\n" +
                                    "\n" +
                                    "5. Originals are welcome.\n" +
                                    "\n" +
                                    "6. Judging will be based on overall group performance.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openUpBS(View view)
    {
        final SpannableString s = new SpannableString("Sahil : 8984888461" + "\n"+ "\n" + "Aditya : 8013706561");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

}
