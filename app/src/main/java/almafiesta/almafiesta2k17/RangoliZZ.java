package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class RangoliZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rangoli_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView77);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }


    public void openRangoliRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(RangoliZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. This is a Group rangoli making event.\n\n" +
                                    "2. Team Size: 1–4 members.\n\n" +
                                    "3. All participants must carry the identity card of their respective colleges.\n\n" +
                                    "4. Participants without identity cards will not be allowed to participate.\n\n" +
                                    "5. Winners in Fine arts genre will receive cash prize of 5k and goodies worth 30k.\n\n" +
                                    "6. Each team will be given maximum 3 hours to complete their rangoli.\n\n" +
                                    "7. Use of any outside acts and internet is strictly prohibited.\n\n" +
                                    "8. Materials provided: Colours, pencils, chalk, thread, paper, perforated bottle.\n\n" +
                                    "9. Participants can use any special trick or technique if permitted by the judges.\n\n" +
                                    "10. Judges decision will be FINAL & BINDING on all participants.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openRangoliCo(View view)
    {
        final SpannableString s = new SpannableString("Saurabh : 8280358334" + "\n"+ "\n" + "Binod : 9078814433");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

}
