package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DrishtikonZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drishtikon_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView47);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openDrishtikonRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(DrishtikonZZ.this)
                            .setTitle("RULES")
                            .setMessage("It is an individual competition in which you’ve to express\n" +
                                    "\n" +
                                    "your thoughts on provided topic in simple and effective\n" +
                                    "\n" +
                                    "manner.\n" +
                                    "\n" +
                                    "Rules :\n" +
                                    "\n" +
                                    "Round-1:\n" +
                                    "\n" +
                                    " Randomly a topic will be given to Participants.\n" +
                                    "\n" +
                                    "30 seconds will be provided to think on given Topic.\n" +
                                    "\n" +
                                    "2 minutes will be given for expressing your thoughts\n" +
                                    "\n" +
                                    "and 30 seconds for conclusion.\n" +
                                    "\n" +
                                    "Round-2\n" +
                                    "\n" +
                                    "Debate –\n" +
                                    "\n" +
                                    "All rules and regulations of this round will be dictated just\n" +
                                    "\n" +
                                    "after completion of 1 st round.\n" +
                                    "\n" +
                                    "General Instructions:-\n" +
                                    "\n" +
                                    "1. Negative marking will be given, if time limit exceeds.\n" +
                                    "\n" +
                                    "2. Decision of judges will be final.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }
}
