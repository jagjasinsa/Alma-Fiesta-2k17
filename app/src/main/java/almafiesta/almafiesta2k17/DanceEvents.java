package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DanceEvents extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dance_events);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView) findViewById(R.id.textView35);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);
    }

    public void openRipOut(View view) {

        Intent intent = new Intent(DanceEvents.this,RipOutZZ.class);
        startActivity(intent);

        /*final SpannableString s = new SpannableString("For more details contact \n"+"Aditya Pandey : 7077540312" + "\n" + "Shunham Pawar : 8989637951"
        +"\n\nOr visit our website at \nhttp://almafiesta.com/main/");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());*/

        /*Uri uri = Uri.parse("http://almafiesta.com/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);*/
    }

    public void openTopsyTurvy(View view) {

        Intent intent = new Intent(DanceEvents.this,TopsyTurvy.class);
        startActivity(intent);

        /*final SpannableString s = new SpannableString("For more details contact \n"+"Himaja : 9078814813" + "\n" + "Binod : 9078814433"
                +"\n\nOr visit our website at \nhttp://almafiesta.com/main/");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());*/


       /* Uri uri = Uri.parse("http://almafiesta.com/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);*/
    }

    public void openRabNeBanaDiJodi(View view) {

        Intent intent = new Intent(DanceEvents.this,RabNeBanaDiJodiZZ.class);
        startActivity(intent);

        /*final SpannableString s = new SpannableString("For more details contact \n"+"Aditya Pandey : 7077540312" + "\n" + "Shunham Pawar : 8989637951"
                +"\n\nOr visit our website at \nhttp://almafiesta.com/main/");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());*/
        /*Uri uri = Uri.parse("http://almafiesta.com/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);*/

    }


}

