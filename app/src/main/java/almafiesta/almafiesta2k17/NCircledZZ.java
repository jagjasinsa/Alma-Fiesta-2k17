package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class NCircledZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ncircled_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView64);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openNCircledRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(NCircledZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. Group of maximum 22 members have to perform a street play of any genre.\n\n" +
                                    "2. The maximum time permitted for each play is 30 minutes.\n\n" +
                                    "3. Music accompanists ar included in the team size stated above.\n\n" +
                                    "4. Teams are expected to perform at the open air venue with audience on all sides.\n\n" +
                                    "5. Only live music is allowed. Teams will have to bring their own instruments.\n\n" +
                                    "6. Any vulgarity/obscenity will be strictly penalized, left to the discretion of judges.\n\n" +
                                    "7. Any kind of fluids, live animals, flame, heavy and sharp objects or any other harmful material is STRICTLY not allowed.\n\n" +
                                    "8. Teams will be heavily penalised if they exceed the time limit. \n\n" +
                                    "9. The time limit for includes both stage setup and performance time. The given time duration is from empty stage to empty stage.\n\n" +
                                    "10. Obscenity (at the discretion of judges) of any kind, is not allowed and may lead to disqualification.\n\n" +
                                    "11. Logistics should be self-arranged.\n\n" +
                                    "12. Each group will be judged mainly on performance, script and various other aspects.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openNcircledCo(View view)
    {
        final SpannableString s = new SpannableString("Raghavendra : 9178523187" + "\n"+ "\n" + "Mayank : 9078814443");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

}
