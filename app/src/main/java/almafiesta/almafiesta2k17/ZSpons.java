package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ZSpons extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zspons);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toast.makeText(getApplicationContext(),"Tap photo for facebook link and phone number to call",Toast.LENGTH_SHORT).show();
    }

    public void fbyash(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/kedia.yash09/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbabhinav(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/abhinav.shukla.31586/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fblekha(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/lekha.preethi.3/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
