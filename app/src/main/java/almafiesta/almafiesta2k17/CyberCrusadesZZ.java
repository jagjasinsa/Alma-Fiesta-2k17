package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class CyberCrusadesZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cyber_crusades_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView82);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }


    public void openCyberCrusadesRules(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(CyberCrusadesZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("The games are :\n\n" +
                                    " \n" +
                                    "1.Counter Strike v1.6/Condition Zero - Team of 5\n\n" +
                                    "\n" +
                                    "2. Call of Duty : Modern Warfare - Team of 5\n\n" +
                                    "\n" +
                                    "3. Need for Speed : Most Wanted - Single\n\n" +
                                    "\n" +
                                    "4. FIFA 15 - Single (League matches)\n\n" +
                                    "\n" +
                                    "5. WWE 2k16 - (Will update soon) \n\n" +
                                    "\n" +
                                    "The list will be updated depending upon the availability of other games.\n\n" +
                                    "\n" +
                                    "The participants will be provided with Keyboard and mouse, and if they wish they can bring their own controllers and mouse etc.\n\n" +
                                    "\n" +
                                    "The venue is fixed as SES Building and the room will be fixed shortly.\n\n" +
                                    "\n" +
                                    "At a time there is a room for over 80 participants but it's subjected to change, depending upon the permission from concerned authorities regarding the allocation of rooms.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openCyberCo(View view)
    {
        final SpannableString s = new SpannableString("Soham : 9769017157" + "\n"+ "\n" + "Sachin : 8947963154");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }


}
