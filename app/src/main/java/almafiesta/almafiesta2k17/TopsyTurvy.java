package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class TopsyTurvy extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topsy_turvy);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView) findViewById(R.id.textView88);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }

    public void openTopsyTurvyRules(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(TopsyTurvy.this)
                            .setTitle("RULES")
                            .setMessage("1.There will be two rounds. \n\n" +
                                    "2. Stage setting time: 1.5 minutes (maximum).\n\n" +
                                    "3. Time limit: 5 -10 minutes. Exceeding this will lead to penalization in marks.\n\n" +
                                    "4.  Team limit : 22 + 2 CAs(for light & sound) \n\n" +
                                    "5. All dance forms are allowed. \n\n" +
                                    "6. Use of water, fire and glass is prohibited.\n\n" +
                                    "7. Audio track to be brought in .mp3 format, in a pen drive or a hard disk and should be submitted by group on the registration desk.\n\n" +
                                    "8. Any specific requirements regarding lights and smoke must be discussed with coordinator well in advance.\n\n" +
                                    "9. Use of Voice Over(s) is allowed. \n\n")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openEvCoTopsyTurvy(View view) {


        final SpannableString s = new SpannableString("For more details contact \n"+"Himaja : 9078814813" + "\n" + "Binod : 9078814433"
                +"\n\nOr visit our website at \nhttp://almafiesta.com/");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

    }
}