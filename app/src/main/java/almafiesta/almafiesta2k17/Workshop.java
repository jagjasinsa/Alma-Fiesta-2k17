package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

public class Workshop extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_workshop, container,
                false);
        TextView textView = (TextView) rootView.findViewById(R.id.textView2);
        TextView textView1 = (TextView) rootView.findViewById(R.id.textView41);
        //TextView textView2 = (TextView) rootView.findViewById(R.id.textView42);
        TextView textView3 = (TextView) rootView.findViewById(R.id.textView43);
        //TextView textView4 = (TextView) rootView.findViewById(R.id.textView44);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fo.ttf");
        Typeface face1 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);
        textView1.setTypeface(face1);
        //textView2.setTypeface(face1);
        textView3.setTypeface(face1);
        //textView4.setTypeface(face1);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SpannableString s = new SpannableString("Salsa is one of the most passionate and enthusiastic dance forms which have couples tapping off to music Register quickly as limited couples.\nFor more info contact" +
                        "\n\nVaibhav Mishra : 7077102321");
                Linkify.addLinks(s, Linkify.PHONE_NUMBERS);

                final AlertDialog d = new AlertDialog.Builder(getContext())
                        .setPositiveButton("GOT IT", null)
                        .setMessage(s)
                        .create();

                d.show();

                ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

            }
        });
       /* textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final SpannableString s = new SpannableString("'You do not take a photograph, you make it.' Alma Fiesta presents 'Photography Workshop'. Register and relieve your dreams through photography.  \nFor more details contact:" +
                        "\n\nAbhinav Shukla : 7077095192");
                Linkify.addLinks(s, Linkify.PHONE_NUMBERS);

                final AlertDialog d = new AlertDialog.Builder(getContext())
                        .setPositiveButton("GOT IT", null)
                        .setMessage(s)
                        .create();

                d.show();

                ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());



            }
        });*/
        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final SpannableString s = new SpannableString("Alma Fiesta is proud to rejoin the tech-savvy with the workshop on Android App Development. So young talents now can implement your innovative, unique and wonderful ideas through apps that none ever thought. Join us to be nurtured with the experienced ones who will enhance and sharpen your skills.\nFor more details contact:" +
                        "\n\nAishwarya : 7077100166");
                Linkify.addLinks(s, Linkify.PHONE_NUMBERS);

                final AlertDialog d = new AlertDialog.Builder(getContext())
                        .setPositiveButton("GOT IT", null)
                        .setMessage(s)
                        .create();

                d.show();

                ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

            }
        });
        /*textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final SpannableString s = new SpannableString("Karate workshop just for girls of our country to safeguard themselves from the growing atrocities of society. Register soon for booking your place.\nFor more details contact:" +
                        "\n\nRachana : 7077100920");
                Linkify.addLinks(s, Linkify.ALL);

                final AlertDialog d = new AlertDialog.Builder(getContext())
                        .setPositiveButton("GOT IT", null)
                        .setMessage(s)
                        .create();

                d.show();

                ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

            }
        });*/
        return rootView;
    }
}