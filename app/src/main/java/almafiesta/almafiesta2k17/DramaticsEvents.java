package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DramaticsEvents extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dramatics_events);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView36);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);
    }

    public void openAankhdavath(View view)
    {
        Intent intent = new Intent(DramaticsEvents.this,AakhdavatZZ.class);
        startActivity(intent);
    }

    public void openNCircled(View view)
    {
        Intent intent = new Intent(DramaticsEvents.this,NCircledZZ.class);
        startActivity(intent);
    }

    public void openFaceOff(View view)
    {
        Intent intent = new Intent(DramaticsEvents.this,FaceOffZZ.class);
        startActivity(intent);
    }

}
