package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.eftimoff.viewpagertransformers.CubeOutTransformer;
import com.eftimoff.viewpagertransformers.FlipHorizontalTransformer;
import com.eftimoff.viewpagertransformers.RotateUpTransformer;
import com.eftimoff.viewpagertransformers.StackTransformer;
import com.eftimoff.viewpagertransformers.TabletTransformer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ZTeam extends AppCompatActivity {
    ViewPager mViewPager;
    String file = "FileAll";
    String temp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zteam);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    public void openCheco(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZChecoActivity.class);
        startActivity(intent);
    }

    public void open1(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZGSec.class);
        startActivity(intent);
    }

    public void open2(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZPublicRelations.class);
        startActivity(intent);
    }

    public void open3(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZMediaMarketing.class);
        startActivity(intent);
    }

    public void open4(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZHospital.class);
        startActivity(intent);
    }

    public void open5(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZPronite.class);
        startActivity(intent);
    }

    public void open6(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZEventM.class);
        startActivity(intent);
    }

    public void open7(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZSpons.class);
        startActivity(intent);
    }

    public void open8(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZWebD.class);
        startActivity(intent);
    }

    public void open9(View view)
    {
        Intent intent = new Intent(ZTeam.this,ZDnd.class);
        startActivity(intent);
    }

}