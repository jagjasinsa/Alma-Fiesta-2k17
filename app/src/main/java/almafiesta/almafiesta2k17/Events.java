package almafiesta.almafiesta2k17;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

public class Events extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_events, container,
                false);
        TextView textView = (TextView) rootView.findViewById(R.id.textView1);
        TextView textView1 = (TextView) rootView.findViewById(R.id.textView9);
        TextView textView2 = (TextView) rootView.findViewById(R.id.textView11);
        TextView textView3 = (TextView) rootView.findViewById(R.id.textView8);
        TextView textView4 = (TextView) rootView.findViewById(R.id.textView50);
        TextView textView5 = (TextView) rootView.findViewById(R.id.textView51);
        TextView textView6 = (TextView) rootView.findViewById(R.id.textView52);
        TextView textView7 = (TextView) rootView.findViewById(R.id.textView53);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fo.ttf");
        Typeface face1 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);
        textView1.setTypeface(face1);
        textView2.setTypeface(face1);
        textView3.setTypeface(face1);
        textView4.setTypeface(face1);
        textView5.setTypeface(face1);
        textView6.setTypeface(face1);
        textView7.setTypeface(face1);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MusicEvents.class);
                startActivity(intent);
            }
        });

        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),DanceEvents.class);
                startActivity(intent);
            }
        });

        textView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),DramaticsEvents.class);
                startActivity(intent);
            }
        });

        textView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),LiteraryEvents.class);
                startActivity(intent);
            }
        });

        textView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),FineArtsEvents.class);
                startActivity(intent);
            }
        });

        textView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),QuizzesEvents.class);
                startActivity(intent);
            }
        });


        textView7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),Miscalleneous.class);
                startActivity(intent);
            }
        });




        return rootView;
    }




}