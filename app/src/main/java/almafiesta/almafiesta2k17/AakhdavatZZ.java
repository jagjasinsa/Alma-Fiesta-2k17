package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class AakhdavatZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aakhdavat_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView59);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openRulesAankhdavath(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(AakhdavatZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. Group have to perform a Stage Play of any genre.\n\n" +
                                    "2. Team Limit: 15 members + 3 CAs (for light and sound) \n\n" +
                                    "3. The minimum time for stage play is 15 minutes and maximum time permitted fo r each play is 4-5 minutes.\n\n" +
                                    "4. Language of the play can be Hindi/English. Mime is allowed.\n\n" +
                                    "5. Pre registration is necessary before January 10, 2017.\n\n" +
                                    "6. Recorded vocals are not allowed, only music is allowed\n\n" +
                                    "7. The teams will have to bring their own music in CDs/PDs.\n\n" +
                                    "8. No naked flames or live animals are allowed on stage.\n\n" +
                                    "9. Sound and Lights will be arranged by the Organisation.\n\n" +
                                    "10. No other logistics will be provided from the Organisation. The costumes and logis tics must be self arranged.\n\n" +
                                    "11. Use of foul language or any obscenity will lead to immediate disqualification.\n\n" +
                                    "12. In case of malfunctioning of any equipment provided, group will be allowed to repeat their performance from the last point of disruption. The decision will be as per judges direction fo how to give grace time. \n\n" +
                                    "13. Group’s performance is judged based on the Script, Stage presence, expressions and various other aspects.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }


    public void openMonoCo(View view)
    {
        final SpannableString s = new SpannableString("Aman Meena : N/A" + "\n"+ "\n" + "Aditya Sarkar : 8013706561");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();
    }

}
