package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class LitsPree extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lits_pree);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView68);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);

    }

    public  void openLitspreeRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(LitsPree.this)
                            .setTitle("INFORMATION")
                            .setMessage("It is a team event.\n" +
                                    "\n" +
                                    "\uF0B7 Each team will consist of 2-3 members.\n" +
                                    "\n" +
                                    "\uF0B7 The event consists of a complete assortment of literary rounds (Crosswords,Puzzles, and so on).\n" +
                                    "\n" +
                                    "\uF0B7 Participants must carry their college ID cards. Participants without identity cards will not be allowed for the event.\n" +
                                    "\n" +
                                    "\uF0B7 Cross college teams are allowed.\n" +
                                    "\n" +
                                    "\uF0B7 In case of any dispute, the decision of the organizers will be final and binding on the participants.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }


    public void openLitSpreeCo(View view)
    {
        final SpannableString s = new SpannableString("Ankit : 9078814570" + "\n"+ "\n" + "Mayank : 9078814443");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();
    }

}
