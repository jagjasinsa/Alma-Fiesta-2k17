package almafiesta.almafiesta2k17;

import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ZSettings extends AppCompatActivity {
    TextView textView;
    String s;
    String file = "FileAll";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zsettings);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        textView = (TextView) findViewById(R.id.textView16);
        Typeface face = Typeface.createFromAsset(this.getAssets(), "fonts/fo.ttf");
        textView.setTypeface(face);
        Button button = (Button) findViewById(R.id.button);
        Button button1 = (Button) findViewById(R.id.button2);
        Button button2 = (Button) findViewById(R.id.button3);
        Button button3 = (Button) findViewById(R.id.button5);
        Button button4 = (Button) findViewById(R.id.button6);
        Button button5 = (Button) findViewById(R.id.button7);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s = "1";
                try {
                    Toast.makeText(getApplicationContext(),"Restart application to see changes",Toast.LENGTH_SHORT).show();
                    FileOutputStream fileOutputStream = openFileOutput(file, MODE_PRIVATE);
                    fileOutputStream.write(s.getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s = "2";
                try {
                    Toast.makeText(getApplicationContext(),"Restart application to see changes",Toast.LENGTH_SHORT).show();
                    FileOutputStream fileOutputStream = openFileOutput(file, MODE_PRIVATE);
                    fileOutputStream.write(s.getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s = "3";
                try {
                    Toast.makeText(getApplicationContext(),"Restart application to see changes",Toast.LENGTH_SHORT).show();
                    FileOutputStream fileOutputStream = openFileOutput(file, MODE_PRIVATE);
                    fileOutputStream.write(s.getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s = "4";
                try {
                    Toast.makeText(getApplicationContext(),"Restart application to see changes",Toast.LENGTH_SHORT).show();
                    FileOutputStream fileOutputStream = openFileOutput(file, MODE_PRIVATE);
                    fileOutputStream.write(s.getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s = "5";
                try {
                    Toast.makeText(getApplicationContext(),"Restart application to see changes",Toast.LENGTH_SHORT).show();
                    FileOutputStream fileOutputStream = openFileOutput(file, MODE_PRIVATE);
                    fileOutputStream.write(s.getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                s = "6";
                try {
                    Toast.makeText(getApplicationContext(),"Restart application to see changes",Toast.LENGTH_SHORT).show();
                    FileOutputStream fileOutputStream = openFileOutput(file, MODE_PRIVATE);
                    fileOutputStream.write(s.getBytes());
                    fileOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
