package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class EuphonyZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_euphony_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView) findViewById(R.id.textView70);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }


    public void openEuphonyRules(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(EuphonyZZ.this)
                            .setTitle("RULES")
                            .setMessage("\n" +
                                    "1. Rounds: 2 Rounds – Qualifying and Final\n\n" +
                                    "2. Eligibility: Band (3-10 member)\n\n" +
                                    "3. Each band will be allotted 5 minutes for sound check and 15 minutes for performance. Total on stage time is 20 minutes.\n\n" +
                                    "4. Time limit for the prelims - 20 minutes and for the final round -30 minutes (Amps on to Amps off).\n\n" +
                                    "5. A drum kit, 5-7 microphones and sufficient input lines will be provided on stage. Other instruments to be brought by participants.\n\n" +
                                    "6. Original compositions are welcome.\n\n" +
                                    "7. Medleys and mashups are allowed provided the performance comes under the given time limit.\n\n" +
                                    "8. Obscenity and vulgarity is not allowed in lyrics and in gestures.\n\n" +
                                    "9. Judges’ decision will be final and binding.\n\n" +
                                    "10. No pre-recorded music is allowed. Although, music can be looped in real time.\n\n")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openEvCoEuphony(View view) {


        final SpannableString s = new SpannableString("Shivam : 9473323093" + "\n"+ "\n" + "Himanshu : 9587497460");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

    }

}
