package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class TrackTheTrackZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_the_track_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView)findViewById(R.id.textView72);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }


    public void openTrackTheTrackRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(TrackTheTrackZZ.this)
                            .setTitle("RULES")
                            .setMessage("Rounds: 2 rounds (Qualifying Round and Final Round)\n\n"+
                                    "Eligibility: Solo Performance on Playback Track.  \n\n" +
                                    "1.First round is a knock out round.\n\n" +
                                    "2. In this round, the contestant is free to sing a song of his/her choice.\n\n" +
                                    "3. Based on the performance in this round, participants will be selected for the final round.\n\n" +
                                    "4. Final round includes two performances by each selected contestant –\n\n" +
                                    "5. One of participant’s choices\n\n" +
                                    "6. One of judge’s choices (to be selected from a list of 3 songs provided by the participant)\n\n" +
                                    "7. Every contestant must bring his/her own karaoke tracks.\n\n" +
                                    "8. He/she must bring at least 5 tracks (2 for the participant’s choice rounds and three for the judges’ choice round).\n\n" +
                                    "9. Every contestant must bring his/her own karaoke tracks in a CD/Pen drive.\n\n" +
                                    "10. Language of performance is restricted to Hindi and English only.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openTra(View view)
    {
        final SpannableString s = new SpannableString("Vaibhav : 7854821418" + "\n"+ "\n" + "Abhijeet: 80045928");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

}
