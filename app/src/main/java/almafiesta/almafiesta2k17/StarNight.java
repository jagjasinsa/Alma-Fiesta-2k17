package almafiesta.almafiesta2k17;


import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Calendar;

public class StarNight extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_star_night, container,
                false);
        TextView textView = (TextView) rootView.findViewById(R.id.textView3);
        TextView textView1 = (TextView)rootView.findViewById(R.id.textView12);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fo.ttf");
        textView.setTypeface(face);
        textView1.setTypeface(face);

        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),StarNightFinal.class);
                startActivity(intent);
                /*Uri uri = Uri.parse("http://memories.almafiesta.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);*/
            }
        });

        Button button = (Button)rootView.findViewById(R.id.button80);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String date = "15/0/2017";
                String parts[] = date.split("/");
                int day = Integer.parseInt(parts[0]);
                int month = Integer.parseInt(parts[1]);
                int year = Integer.parseInt(parts[2]);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);


                Toast.makeText(getContext(),"Do not create the event if you have already done",Toast.LENGTH_LONG).show();

                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", calendar.getTimeInMillis());
                intent.putExtra("allDay", true);
                intent.putExtra("endTime", calendar.getTimeInMillis());
                intent.putExtra("title", "Get ready for the Star-Nite, 'THE LOCAL TRAIN' TODAY!!! ");
                startActivity(intent);
            }
        });


        return rootView;

    }
}