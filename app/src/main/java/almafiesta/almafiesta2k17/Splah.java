package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.MediaController;
import android.widget.VideoView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import me.wangyuwei.particleview.ParticleView;

public class Splah extends AppCompatActivity {
    ParticleView particleView;
    VideoView videoView;
    String file = "SplashFile";
    String temp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splah);

        try {
            FileInputStream fileInputStream = openFileInput(file);
            int c;
            while ((c = fileInputStream.read()) != -1) {
                temp = temp + Character.toString((char) c);
            }
            try {
                this.setRequestedOrientation(
                        ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
                particleView = (ParticleView) findViewById(R.id.particleview);
                particleView.startAnim();
                particleView.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
                    @Override
                    public void onAnimationEnd() {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(Splah.this, Home.class);
                                startActivity(intent);
                                finish();
                            }

                        }, 1000);

                    }
                });


            } catch (Exception e1) {

            }

        } catch (FileNotFoundException e) {
            String s = "zeus";
            FileOutputStream fout = null;
            try {
                fout = openFileOutput(file, MODE_APPEND);
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            try {
                fout.write(s.getBytes());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            try {
                fout.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            /*videoView = (VideoView) findViewById(R.id.video_view);
            MediaController mediaController = new MediaController(this);
            mediaController.setAnchorView(videoView);
            videoView.setMediaController(null);


            videoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.vid));
            videoView.start();
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    Intent intent = new Intent(Splah.this, Home.class);
                    startActivity(intent);
                    finish();
                }
            });*/
            Intent intent = new Intent(Splah.this,Splash2.class);
            startActivity(intent);
            finish();


        } catch (IOException e) {
            e.printStackTrace();
        }



        /*this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        particleView = (ParticleView)findViewById(R.id.particleview);
        particleView.startAnim();
        particleView.setOnParticleAnimListener(new ParticleView.ParticleAnimListener() {
            @Override
            public void onAnimationEnd() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(Splah.this,Home.class);
                        startActivity(intent);
                        finish();
                    }

                }, 1000);

            }
        });*/

    }

    public void skipit(View view) {
        videoView.setOnCompletionListener(null);
        Intent intent = new Intent(Splah.this, Home.class);
        startActivity(intent);
        finish();
    }
}
