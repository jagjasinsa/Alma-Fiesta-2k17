package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Miscalleneous extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miscalleneous);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView40);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);
    }


    public void openInformals(View view)
    {
        Intent intent = new Intent(Miscalleneous.this,InformalsZZ.class);
        startActivity(intent);
    }

    public void openCyberCrusades(View view)
    {
        Intent intent = new Intent(Miscalleneous.this,CyberCrusadesZZ.class);
        startActivity(intent);
    }

    public void openYouthMarathon(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(Miscalleneous.this)
                            .setTitle("INFORMATION")
                            .setMessage("An event under Prayatna. Run for a cause.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

}
