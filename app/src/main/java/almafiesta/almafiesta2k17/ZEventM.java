package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ZEventM extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zevent_m);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toast.makeText(getApplicationContext(),"Scroll Down for more",Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(),"Tap photo for facebook link and phone number to call",Toast.LENGTH_SHORT).show();
    }

    public void fbvaibhav(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/vaibhavvmstar/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbaishwarya(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/aishwarya.chaturvedi.564/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbmayur(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/mayur.agrawal.9235/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbbibek(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/bibek.mallik.1/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
