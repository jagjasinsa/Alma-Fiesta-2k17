package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SeedhaSamwadZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seedha_samwad_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView45);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void rulesOfSeedhaSamvaad(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(SeedhaSamwadZZ.this)
                            .setTitle("RULES")
                            .setMessage("Rules:\n" +
                                    "\n" +
                                    "Round-1\n" +
                                    "\n" +
                                    "Group Discussion –\n" +
                                    "\n" +
                                    "\uF0B7 All participants will be divided into groups. Each\n" +
                                    "\n" +
                                    "group will consist of 4 individuals.\n" +
                                    "\n" +
                                    "\uF0B7 Any current topic will be provided for discussion.\n" +
                                    "\n" +
                                    "\uF0B7 2 minutes will be provided for thinking and 10\n" +
                                    "\n" +
                                    "minutes for Group Discussion.\n" +
                                    "\n" +
                                    "\uF0B7 According to the performance in 1 st round 10\n" +
                                    "\n" +
                                    "participants will be selected for 2 nd round.\n" +
                                    "\n" +
                                    "Round-2\n" +
                                    "\n" +
                                    "Debate –\n" +
                                    "\n" +
                                    "All rules and regulations of this round will be dictated just\n" +
                                    "\n" +
                                    "after completion of 1 st round.\n" +
                                    "\n" +
                                    "General Instructions:-\n" +
                                    "\n" +
                                    "1. Negative marking will be given, if time limit exceeds.\n" +
                                    "\n" +
                                    "2. Decision of judges will be final.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

}
