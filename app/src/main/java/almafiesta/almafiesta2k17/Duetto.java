package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Duetto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_duetto);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView) findViewById(R.id.textView89);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }


    public void openDuettoRules(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(Duetto.this)
                            .setTitle("RULES")
                            .setMessage("Rounds: 2 rounds  – Qualifying and Final\n\n" +
                                    "Eligibility: Duet Performance on Karaoke Track\n\n" +

                                    "1. First round is a knock out round. In this round, the contestants are free to sing a song of their choice.\n\n" +

                                    "2. Based on the performance in this round, groups will be selected for the final round.\n\n" +

                                    "3. Final round includes one performance by each selected group which will be of judges’ choice (to be selected from a list of 3 songs provided by the group).\n\n" +

                                    "4. Every group must bring their own karaoke tracks.\n\n" +

                                    "5. Each participating group must bring at least 4 tracks (1 for the contestant’s choice rounds and three for the judges’ choice round).\n\n" +

                                    "6. Participants must bring their own playback/karaoke track in a CD/USB drive.\n\n" +

                                    "7. Language of performance is restricted to Hindi and English only.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }
}
