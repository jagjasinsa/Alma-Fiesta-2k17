package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ZPublicRelations extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zpublic_relations);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toast.makeText(getApplicationContext(),"Tap photo for facebook link and phone number to call",Toast.LENGTH_SHORT).show();
    }

    public void fbShobhitA(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/shobhit.agrawal.1257/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void fbJaiswal(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/abhishek.iitbbsr/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void fbSammera(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/sameera.durbha/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
