package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class ZWebD extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zweb_d);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Toast.makeText(getApplicationContext(),"Scroll down for more",Toast.LENGTH_SHORT).show();Toast.makeText(getApplicationContext(),"Tap photo for facebook link and phone number to call",Toast.LENGTH_SHORT).show();
    }

    public void fbkansi(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/kanisque/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbzeus(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/zsusalpha/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbavd(View view)
    {
        Uri uri = Uri.parse("http://www.facebook.com/avdheshkp/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbshobitg(View view)
    {
        Uri uri = Uri.parse("http:/www.facebook.com/shobhit.datta/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
    public void fbabijay(View view)
    {
        Uri uri = Uri.parse("https://www.facebook.com/profile.php?id=100004675909399/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
