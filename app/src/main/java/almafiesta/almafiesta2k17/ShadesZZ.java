package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class ShadesZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shades_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView78);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openShadzRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(ShadesZZ.this)
                            .setTitle("RULES")
                            .setMessage("It is a solo pencil sketching event.\n" +
                                    "\n" +
                                    "\uF0B7 Each participant will be given maximum of 2 hours to complete their sketch.\n" +
                                    "\n" +
                                    "\uF0B7 Winners in Finearts genre will receive cash prize of 5k and goodies worth 30k.\n" +
                                    "\n" +
                                    "\uF0B7 All participants must bring their college identity cards.\n" +
                                    "\n" +
                                    "\uF0B7 Participants without identity cards will not be allowed to participate.\n" +
                                    "\n" +
                                    "\uF0B7 Any pencil sketch is allowed in the event.\n" +
                                    "\n" +
                                    "\uF0B7 Materials provided: Drawing sheets, Pencils, Erasers and Sharpeners\n" +
                                    "\n" +
                                    "\uF0B7 The decision of the judge shall be FINAL & BINDING.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openShadesCo(View view)
    {
        final SpannableString s = new SpannableString("Aman Meena : 9078814795" + "\n"+ "\n" + "Niteesh : 9078814443");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

}
