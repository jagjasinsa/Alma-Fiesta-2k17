package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class ParliamentaryDebate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parliamentary_debate);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView)findViewById(R.id.textView76);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }

    public void openParliamentaryDebateRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(ParliamentaryDebate.this)
                            .setTitle("RULES")
                            .setMessage("*Asian Parliamentary debate format will be followed*\n\n" +
                                    "1. Topic of the Debate will be announced on the website and the Facebook page.\n\n" +
                                    "2. Teams: Each debating match will consist of two teams.\n\n" +
                                    "3. Team size: Exactly 3\n\n" +
                                    "4. Time of Speech : Constructive Speeches: 6 + 1 minutes. \n\n" +
                                    "5. Reply Speeches: 3 + 1 minutes.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }


    public void openParliamentaryDebateCo(View view)
    {
        final SpannableString s = new SpannableString("Chaitanya : 9589683331" + "\n"+ "\n" + "Himanshu : 9587497460");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();
        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }

}

