package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class InformalsZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informals_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView) findViewById(R.id.textView98);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openSumo(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Ever wondered how does it feel to be a sumo wrestler? Here's your chance.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });

    }

    public void openZorbing(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Inside a bubble and off you go.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openArm(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Come and show us your strength.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openFull(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Have you seen that TV show? Recreating memories by having another version of that.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openBrick(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Are you a Civil Engineer?")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openShoot(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Strike a 3-pointer.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openPaint(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(InformalsZZ.this)
                            .setTitle("INFORMATION")
                            .setMessage("Get entry into a real life Counter Strike.")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }


}
