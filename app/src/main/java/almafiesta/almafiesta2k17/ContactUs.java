package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

public class ContactUs extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contact_us, container,
                false);
        TextView textView = (TextView) rootView.findViewById(R.id.textView4);
        TextView textView1 = (TextView) rootView.findViewById(R.id.textView5);
        TextView textView2 = (TextView) rootView.findViewById(R.id.textView6);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/fo.ttf");
        textView.setTypeface(face);
        textView1.setTypeface(face);
        textView2.setTypeface(face);
        textView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ZSettings.class);
                startActivity(intent);
            }
        });
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ZTeam.class);
                startActivity(intent);
            }
        });
        return rootView;
    }


}