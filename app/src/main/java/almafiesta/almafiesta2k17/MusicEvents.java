package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MusicEvents extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_events);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView)findViewById(R.id.textView34);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fz.ttf");
        textView7.setTypeface(face);
    }

    public void openEuphony(View view)
    {
        Intent intent = new Intent(MusicEvents.this,EuphonyZZ.class);
        startActivity(intent);
    }


    public void openTrackTheTrack(View view)
    {
        Intent intent = new Intent(MusicEvents.this,TrackTheTrackZZ.class);
        startActivity(intent);
    }

    public  void openUpBeat(View view)
    {
        Intent intent = new Intent(MusicEvents.this,UpBeatZZ.class);
        startActivity(intent);
    }

    public void openUnplugged(View view)
    {
        Intent intent = new Intent(MusicEvents.this,UnpluggedZZ.class);
        startActivity(intent);
    }

    public void openDuetto(View view)
    {
        Intent intent = new Intent(MusicEvents.this,Duetto.class);
        startActivity(intent);
    }

}
