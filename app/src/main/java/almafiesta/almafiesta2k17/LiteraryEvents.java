package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LiteraryEvents extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_literary_events);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView37);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);

    }

    public  void seedhasamwadopen(View view)
    {
        Intent intent = new Intent(LiteraryEvents.this,SeedhaSamwadZZ.class);
        startActivity(intent);
    }

    public void drishtikonopen(View view)
    {
        Intent intent = new Intent(LiteraryEvents.this,DrishtikonZZ.class);
        startActivity(intent);
    }

    public void openLitSpree(View view)
    {
        Intent intent = new Intent(LiteraryEvents.this,LitsPree.class);
        startActivity(intent);
    }


    public void openPerspective(View view)
    {
        Intent intent = new Intent(LiteraryEvents.this,PerspectiveZZ.class);
        startActivity(intent);
    }

    public void openParliamentaryDebate(View view)
    {
        Intent intent = new Intent(LiteraryEvents.this,ParliamentaryDebate.class);
        startActivity(intent);
    }

    public void openMun(View view)
    {
        Uri uri = Uri.parse("http://iitbbsrmun.almafiesta.com/"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
