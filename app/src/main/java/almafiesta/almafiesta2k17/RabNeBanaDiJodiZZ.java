package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class RabNeBanaDiJodiZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rab_ne_bana_di_jodi_zz);
        TextView textView7 = (TextView) findViewById(R.id.textView86);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }


    public void openRabNeBanaDiJodiRules(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()) {
                    new AlertDialog.Builder(RabNeBanaDiJodiZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. There will be a single round.\n" +
                                    "2. The duo couple will be given 2-6 minutes to perform.\n\n" +
                                    "3. The audio track must be submitted in a pen drive or hard disk in .mp3 format, at the time of confirmation of registration at the event venue.\n\n" +
                                    "4. All participants must bring their college identity cards.\n\n" +
                                    "5. Song must be brought by the participating team in a CD, DVD or Pen drive if they don’t, disqualified.\n\n" +
                                    "6. The songs must not contain any illicit or offensive language.\n\n" +
                                    "7. Any obscene action in the dance is prohibited and will lead to Disqualification.\n\n" +
                                    "8. All props, costumes and make up will have to be brought by the teams.\n\n" +
                                    "9. No props will be provided by the organizers. In case of special prop requirement such as chairs, the teams will have to inform the organizers in advance.\n\n" +
                                    "10 Use of flame or fluid on stage, lighting of candles, matches or cigarettes is strictly NOT allowed and will lead in Disqualification of the team\n\n")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openRabNBDJEVCO(View view) {
        final SpannableString s = new SpannableString("For more details contact \n" + "Aditya Pandey : 7077540312" + "\n" + "Shunham Pawar : 8989637951"
                + "\n\nOr visit our website at \nhttp://almafiesta.com/");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }
}
