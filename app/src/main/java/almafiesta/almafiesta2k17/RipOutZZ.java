package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class RipOutZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rip_out_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView7 = (TextView) findViewById(R.id.textView84);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView7.setTypeface(face);
    }

    public void openRulesRipOut(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(RipOutZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. There will be a single round. \n\n" +
                                    "2. The individual will be given 3-6 minutes to perform. \n\n" +
                                    "3. The individual must bring the audio track in a pendrive or hard disk in .mp3 format and submit it at the time of confirmation of registration at the event venue.\n\n" +
                                    "4. There will be two winners – one for each category (male and female). All participants must bring their college identity cards. Participants without identity cards will not be allowed to participate.\n\n" +
                                    "5. Use of flame or fluid on stage, lighting of candles, matches or cigarettes is strictly NOT allowed and will lead in Disqualification.\n\n" +
                                    "6.  Participants have to arrange for their own props, costumes & make-up. No props will be provided by the organizers. However, in case of special prop requirement such as chairs the participants will have to inform the organizers in advance.\n\n" +
                                    "7. In case of malfunctioning of any equipment provided, the participant will be allowed to repeat the performance or start from the point of disruption as he/she chooses, with the consent of the judges.")
                            .setCancelable(false)
                            .setPositiveButton("GOT IT", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openevRipOut(View view)
    {
        final SpannableString s = new SpannableString("For more details contact \n"+"Aditya Pandey : 7077540312" + "\n" + "Shunham Pawar : 8989637951"
                +"\n\nOr visit our website at \nhttp://almafiesta.com/");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }


}
