package almafiesta.almafiesta2k17;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.TextView;

public class BlindStrokesZZ extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blind_strokes_zz);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView79);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fj.ttf");
        textView.setTypeface(face);
    }

    public void openBlindStrokesRules(View view)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (!isFinishing()){
                    new AlertDialog.Builder(BlindStrokesZZ.this)
                            .setTitle("RULES")
                            .setMessage("1. It is a team event.\n\n" +
                                    "\n" +
                                    "2. Team Size : 2 members\n\n" +
                                    "\n" +
                                    "3. Time limit: 2 hours\n\n" +
                                    "\n" +
                                    "4. Winners in Finearts genre will receive cash prize of 5k and goodies worth 30k.\n\n" +
                                    "\n" +
                                    "5. All participants must bring their college identity cards.\n\n" +
                                    "\n" +
                                    "6. Participants without identity cards will not be allowed to participate.\n\n" +
                                    "\n" +
                                    "7. One participant per team will be blindfolded.\n\n" +
                                    "\n" +
                                    "8. The blindfolded participant will have to draw a given picture based on the verbal instructions of his/her team mate.\n\n" +
                                    "\n" +
                                    "9. Materials provided : A2 sheets, Pens\n\n" +
                                    "\n" +
                                    "10. Participants can use any special trick or technique if judges permit.\n\n" +
                                    "\n" +
                                    "11. Judges decision will be FINAL &amp; BINDING on all participants")
                            .setCancelable(false)
                            .setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            }).show();
                }
            }
        });
    }

    public void openBlindCo(View view)
    {
        final SpannableString s = new SpannableString("Ankit : 9078814570" + "\n"+ "\n" + "Shubham P : 8989637351");
        Linkify.addLinks(s, Linkify.ALL);

        final AlertDialog d = new AlertDialog.Builder(this)
                .setPositiveButton("GOT IT", null)
                .setMessage(s)
                .create();

        d.show();

        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
    }



}
