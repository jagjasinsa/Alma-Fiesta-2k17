package almafiesta.almafiesta2k17;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

public class FineArtsEvents extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fine_arts_events);
        this.setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        TextView textView = (TextView)findViewById(R.id.textView38);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/fz.ttf");
        textView.setTypeface(face);

    }

    public void openRangoli(View view)
    {
        Intent intent = new Intent(FineArtsEvents.this,RangoliZZ.class);
        startActivity(intent);
    }


    public void openShadz(View view)
    {
        Intent intent = new Intent(FineArtsEvents.this,ShadesZZ.class);
        startActivity(intent);
    }

    public void openBlindStrokes(View view)
    {
        Intent intent = new Intent(FineArtsEvents.this,BlindStrokesZZ.class);
        startActivity(intent);
    }

}
